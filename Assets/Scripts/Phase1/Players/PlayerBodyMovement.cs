﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBodyMovement : PlayerMovement
{

    public int followingSpeed = 2;
    public PlayerMovement bodyParent;

    // Start is called before the first frame update
    void Start()
    {
        PlayerSprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        LastPosition = transform;

        if (bodyParent.IsMoving)
        {
            IsMoving = true;

            transform.position = Vector3.Lerp(transform.position, bodyParent.transform.position, Time.deltaTime * 20);
        }
        else
        {
            IsMoving = false;
        }
        
    }
}
