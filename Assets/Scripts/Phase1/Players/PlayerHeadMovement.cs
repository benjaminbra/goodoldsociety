﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHeadMovement : PlayerMovement
{
    public int movementSpeed = 2;
    public Phase1MasterHandler GameMasterHandler;

    // Start is called before the first frame update
    void Start()
    {
        PlayerSprite = GetComponent<SpriteRenderer>();
        LastPosition = transform;
    }

    // Update is called once per frame
    void Update()
    {
        GameMasterHandler.transform.position = transform.position;
        IsMoving = false;
        var newPosition = transform.position;
        if (Input.GetAxis("Vertical") > 0)
        { // Cool j'avance
            IsMoving = true;
            newPosition += Time.deltaTime * movementSpeed * transform.up;

        }
        else if (Input.GetAxis("Vertical") < 0)
        {
            IsMoving = true;
            newPosition += Time.deltaTime * movementSpeed * -1 * transform.up;
        }

        if (Input.GetAxis("Horizontal") < 0) 
        { // Go à gauche
            IsMoving = true;
            newPosition += Time.deltaTime * movementSpeed * -1 * transform.right;
        }else if (Input.GetAxis("Horizontal") > 0)
        { // Go à droite
            IsMoving = true;
            newPosition += Time.deltaTime * movementSpeed * transform.right;
                
        }

        if (IsMoving)
        {
            LastPosition = transform;
            transform.position = newPosition;
        }
        
    }

    
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<FoodEating>())
        {
            FoodEating eating = other.GetComponent<FoodEating>();
            if (eating.isAlive)
            {
                eating.isAlive = false;
                eating.Renderer.sprite = null;
                if (eating.isVeggies)
                {
                    GameMasterHandler.addVeggies(); 
                }
                else
                {
                    GameMasterHandler.addMeat();
                }
            
            }
        }
    }
}
