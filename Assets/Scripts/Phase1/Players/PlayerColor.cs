﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerColor : MonoBehaviour
{
    public Color entityColor;
    public SpriteRenderer parentSprite;
    
    private SpriteRenderer _bodySprite;
    private bool _isparentSpriteNotNull;

    // Start is called before the first frame update
    void Start()
    {
        _isparentSpriteNotNull = parentSprite != null;
        _bodySprite = this.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isparentSpriteNotNull)
        {
            _bodySprite.color = parentSprite.color;
        } else
        {
            _bodySprite.color = entityColor;
        }
    }
}
