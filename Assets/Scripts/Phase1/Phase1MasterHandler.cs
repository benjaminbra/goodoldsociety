﻿using Enums;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Phase1MasterHandler : MonoBehaviour
{

    public GameDataHandler GameDataHandler;

    public GameObject carnivoreSprite;
    public GameObject omnivoreSprite;
    public GameObject herbivoreSprite;

    public Image FoodLevel;
    
    public float actualTimer;
    public float maxTimerSecondes;
    public RectTransform aiguille;

    public Transform foodCursor;
    public RectTransform foodBar;
    public float actualPosition;

    public GameObject vegatableSpawnerPrefab;
    public GameObject meatSpawnerPrefab;

    private CircleCollider2D _circleCollider2D;
    
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("addSecondAtTimer", 1f, 1f);
        _circleCollider2D = GetComponent<CircleCollider2D>();

        addFoodInSpace(vegatableSpawnerPrefab);
        addFoodInSpace(vegatableSpawnerPrefab);
        addFoodInSpace(vegatableSpawnerPrefab);
        addFoodInSpace(vegatableSpawnerPrefab);
        addFoodInSpace(vegatableSpawnerPrefab);
        addFoodInSpace(vegatableSpawnerPrefab);
        addFoodInSpace(vegatableSpawnerPrefab);
        addFoodInSpace(vegatableSpawnerPrefab);
        addFoodInSpace(vegatableSpawnerPrefab);
        addFoodInSpace(vegatableSpawnerPrefab);
        addFoodInSpace(vegatableSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
        addFoodInSpace(meatSpawnerPrefab);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameDataHandler.foodLevel == 0)
        {
            GameDataHandler.IsExctinction = true;
            SceneManager.LoadScene("Ending");
        }
        
        if (actualTimer >= maxTimerSecondes)
        {
            SceneManager.LoadScene("Phase2");
        }
        int aiguilleRotation = Mathf.RoundToInt(-1f * ((actualTimer/maxTimerSecondes)*360f));
        aiguille.localEulerAngles = new Vector3(0,0, aiguilleRotation);

        float width = foodBar.rect.width;
        float posX = actualPosition /100 * width;

        foodCursor.localPosition = new Vector3(posX, foodCursor.localPosition.y, 0);

        FoodLevel.fillAmount = (float)GameDataHandler.foodLevel / (float)GameDataHandler.maxFoodLevel;
        
        if (actualPosition < 33)
        {
            GameDataHandler.foodChoice = FoodChoiceEnum.Herbivore;
            carnivoreSprite.SetActive(false);
            omnivoreSprite.SetActive(false);
            herbivoreSprite.SetActive(true);
        }
        else if(actualPosition > 66)
        {
            GameDataHandler.foodChoice = FoodChoiceEnum.Carnivore;
            carnivoreSprite.SetActive(true);
            omnivoreSprite.SetActive(false);
            herbivoreSprite.SetActive(false);
            
        } else
        {
            GameDataHandler.foodChoice = FoodChoiceEnum.Omnivore;
            carnivoreSprite.SetActive(false);
            omnivoreSprite.SetActive(true);
            herbivoreSprite.SetActive(false);
        }
    }

    void addSecondAtTimer()
    {
        actualTimer += 1;
        GameDataHandler.updateFood(-4);
    }

    public void addVeggies()
    {
        if (actualPosition > 0)
        {
            GameDataHandler.updateFood(3);
            actualPosition--;
        }
    }

    public void addMeat()
    {
        if (actualPosition < 100)
        {
            GameDataHandler.updateFood(5);
            actualPosition++;
        }
    }

    private void addFoodInSpace(GameObject prefab)
    {
        GameObject newObject = prefab;
        float a = Random.value * 2f * Mathf.PI;
        float r = (_circleCollider2D.radius * transform.localScale.x) * Mathf.Sqrt(Random.value);

        // If you need it in Cartesian coordinates
        float x = r * Mathf.Cos(a) + transform.position.x;
        float y = r * Mathf.Sin(a) + transform.position.y;


        Instantiate(newObject, new Vector3(x,y,0), Quaternion.identity);

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        
        if (other.name.Contains("FoodVegetables"))
        {
            addFoodInSpace(vegatableSpawnerPrefab);
        }
        else if(other.name.Contains("FoodMeats"))
        {
            addFoodInSpace(meatSpawnerPrefab);
        }
        Destroy(other.gameObject);
    }
}
