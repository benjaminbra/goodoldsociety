﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class FoodSpawner : MonoBehaviour
{
    public List<FoodEating> leftAnchors;
    public List<FoodEating> rightAnchors;
    public List<Sprite> leftSpriteFood;
    public List<Sprite> rightSpriteFood;


    public int rangeRandom = 3;
    // Start is called before the first frame update
    void Start()
    {
        
        for (int i = 0; i < leftAnchors.Count; i++)
        {
            if (Random.Range(0, rangeRandom) == 0)
            {
                FoodEating anchor = leftAnchors[i];
                leftAnchors[i] = anchor;
                anchor.isAlive = true;
                anchor.Renderer.sprite = leftSpriteFood[Random.Range(0,leftSpriteFood.Count)];
            }

        }
        
        for (int i = 0; i < rightAnchors.Count; i++)
        {
            if (Random.Range(0, rangeRandom) == 0)
            {
                rightAnchors[i].isAlive = true;
                rightAnchors[i].Renderer.sprite = rightSpriteFood[Random.Range(0,rightSpriteFood.Count)];
            }

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
