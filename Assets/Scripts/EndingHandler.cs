﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndingHandler : MonoBehaviour
{
    public GameDataHandler GameDataHandler;
    public GameObject apocalypse;
    public GameObject exctinction;
    public GameObject dictature;
    public GameObject famine;
    public GameObject future;
    public GameObject densePollution;
    public GameObject village;

    public EndingDialogu EndingDialogu;
    
    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.FindGameObjectWithTag("GameDatas"))
        {
            GameDataHandler = GameObject.FindGameObjectWithTag("GameDatas").GetComponent<GameDataHandler>();
        }
        if (GameDataHandler.IsViolence)
        {
            apocalypse.SetActive(true);
            EndingDialogu.message.text = "Après des années d'évolutions et d'existence, votre espèce s'est anéantie elle-même...";
        }
        else if(GameDataHandler.IsExctinction)
        {
            exctinction.SetActive(true);
            EndingDialogu.message.text = "Pas de chance... Votre espèce n'était pas faite pour ce monde de toutes façons...";
        } 
        else if (GameDataHandler.IsFamine)
        {
            famine.SetActive(true);
            EndingDialogu.message.text = "Il n'y a rien de plus cruel que de laisser son espèce mourir de faim. " +
                                         "\r\nVous avez échoué à votre tâche de Leader !";
        } 
        else if (GameDataHandler.IsDictator)
        {
            dictature.SetActive(true);
            EndingDialogu.message.text = "C'était sûrement la meilleure solution." +
                                         "\r\nLes gens ont besoin d'un Leader Suprême pour les guider et leur montrer le bon chemin !";
        } 
        else if (!GameDataHandler.IsHighDensity)
        {
            village.SetActive(true);
            EndingDialogu.message.text = "Votre espèce a su vivre paisiblement pendant des années, et va continuer à le faire.";
        }
        else if (GameDataHandler.isPollution && GameDataHandler.IsHighDensity)
        {
            densePollution.SetActive(true);
            EndingDialogu.message.text = "La pollution... les gens entassés... " +
                                         "\r\nComment en êtes-vous arrivez là ?";
        } 
        else if (GameDataHandler.IsHighDensity && !GameDataHandler.isPollution)
        {
            future.SetActive(true);
            EndingDialogu.message.text = "Le futur... des gens entassés... des néons partout..." +
                                         "\r\nÇa  aurait pu être pire. Au moins les gens sont heureux et en bonne santé.";
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetBackToMainMenu()
    {
        SceneManager.MoveGameObjectToScene(GameDataHandler.gameObject, SceneManager.GetActiveScene());
        Destroy(GameDataHandler.gameObject);
        SceneManager.LoadScene("Credits");
    }
}
