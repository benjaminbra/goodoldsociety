﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Phase4MayorCreation
{
    public class MayorCreationHandler : MonoBehaviour
    {
        public Image jacket;
        public Image skin;
        public Image shirt;
        public Image stuff;
        public Image face;
        public Image head;
        public Image hair;

        public Sprite[] stuffs;
        public Sprite[] faces;
        public Sprite[] heads;
        public Sprite[] hairs;

        public Color[] skinColors;
        public Color[] hairColors;
        public Color[] stuffColors;
        
        public TextMeshProUGUI partText;
        public TextMeshProUGUI modelText;
        public TextMeshProUGUI colorText;

        public int selectedStuff = 0;
        public int selectedFace = 0;
        public int selectedHead = 0;
        public int selectedHair = 0;
        
        public int selectedColorStuff = 0;
        public int selectedColorJacket = 0;
        public int selectedColorShirt = 0;
        public int selectedColorSkin = 0;
        public int selectedColorHair = 0;
        
        public int selectedPart = 0;
        public int selectedModel = 0;
        public int selectedColor = 0;

        private MayorPart[] _partList;
    
        // Start is called before the first frame update
        void Start()
        {
            _partList = new MayorPart[7];
            _partList[0] = new MayorPart("Veste",jacket);
            _partList[1] = new MayorPart("Peau",skin);
            _partList[2] = new MayorPart("Haut",shirt);
            _partList[3] = new MayorPart("Accessoire",stuff);
            _partList[4] = new MayorPart("Visage",face);
            _partList[5] = new MayorPart("Tête",head);
            _partList[6] = new MayorPart("Cheveux",hair);
            
            partText.text = _partList[selectedPart].PartName;
            getModel(false, true);
            getColor(false, true);
            updateModelName();
            updateColorName();
            generalColorUpdate();
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        public void nextPart()
        {
            selectedPart++;
            if (selectedPart > _partList.Length - 1)
            {
                selectedPart = 0;
            }

            partText.text = _partList[selectedPart].PartName;
            getModel(false, true);
            updateModelName();
            getColor(false, true);
            updateColorName();
        }

        public void previousPart()
        {
            selectedPart--;
            if (selectedPart < 0)
            {
                selectedPart = _partList.Length - 1;
            }
            
            partText.text = _partList[selectedPart].PartName;
            getModel(false, true);
            updateModelName();
            getColor(false, true);
            updateColorName();
        }

        public void nextModel()
        {
            selectedModel++;
            if (selectedModel > getModel(false, false).Length - 1)
            {
                selectedModel = 0;
            }
            updateModel();
            updateModelName();
        }

        public void previousModel()
        {
            selectedModel--;
            if (selectedModel < 0)
            {
                selectedModel = getModel(false, false).Length - 1;
            }
            updateModel();
            updateModelName();
        }

        public void nextColor()
        {
            selectedColor++;
            if (selectedColor > getColor(false, false).Length - 1)
            {
                selectedColor = 0;
            }

            updateColor();
            updateColorName();
            generalColorUpdate();
        }

        public void previousColor()
        {
            selectedColor--;
            if (selectedColor < 0)
            {
                selectedColor = getColor(false, false).Length - 1;
            }

            updateColor();
            updateColorName();
            generalColorUpdate();
        }

        private Color[] getColor(bool updateSelected, bool updateModelSelection)
        {
            Color[] list;
            switch (selectedPart)
            {
                case 0:
                    list = stuffColors;
                    if (updateSelected)
                    {
                        selectedColorJacket = selectedColor;
                    }
                    if (updateModelSelection)
                    {
                        selectedColor = selectedColorJacket;
                    }
                    break;
                case 1:
                    list = skinColors;
                    if (updateSelected)
                    {
                        selectedColorSkin = selectedColor;
                    }
                    if (updateModelSelection)
                    {
                        selectedColor = selectedColorSkin;
                    }
                    break;
                case 2:
                    list = stuffColors;
                    if (updateSelected)
                    {
                        selectedColorShirt = selectedColor;
                    }
                    if (updateModelSelection)
                    {
                        selectedColor = selectedColorShirt;
                    }
                    break;
                case 3:
                    list = stuffColors;
                    if (updateSelected)
                    {
                        selectedColorStuff = selectedColor;
                    }
                    if (updateModelSelection)
                    {
                        selectedColor = selectedColorStuff;
                    }
                    break;
                case 4:
                    list = hairColors;
                    if (updateSelected)
                    {
                        selectedColorHair = selectedColor;
                    }
                    if (updateModelSelection)
                    {
                        selectedColor = selectedColorHair;
                    }
                    break;
                case 5:
                    list = skinColors;
                    if (updateSelected)
                    {
                        selectedColorSkin = selectedColor;
                    }
                    if (updateModelSelection)
                    {
                        selectedColor = selectedColorSkin;
                    }
                    break;
                case 6:
                    list = hairColors;
                    if (updateSelected)
                    {
                        selectedColorHair = selectedColor;
                    }
                    if (updateModelSelection)
                    {
                        selectedColor = selectedColorHair;
                    }
                    break;
                default:
                    list = new Color[0];
                    break;
            }

            return list;
        }
        
        private Sprite[] getModel(bool updateSelected, bool updateModelSelection)
        {
            Sprite[] list;
            switch (selectedPart)
            {
                case 3:
                    list = stuffs;
                    if (updateSelected)
                    {
                        selectedStuff = selectedModel;
                    }

                    if (updateModelSelection)
                    {
                        selectedModel = selectedStuff;
                    }
                    
                    break;
                case 4:
                    list = faces;
                    if (updateSelected)
                    {
                        selectedHair = selectedModel;
                    }

                    if (updateModelSelection)
                    {
                        selectedModel = selectedHair;
                    }
                    break;
                case 5:
                    list = heads;
                    if (updateSelected)
                    {
                        selectedHead = selectedModel;
                    }

                    if (updateModelSelection)
                    {
                        selectedModel = selectedHead;
                    }
                    break;
                case 6:
                    list = hairs;
                    if (updateSelected)
                    {
                        selectedHair = selectedModel;
                    }

                    if (updateModelSelection)
                    {
                        selectedModel = selectedHair;
                    }

                    break;
                default:
                    list = new Sprite[0];
                    break;
            }

            return list;
        }

        private void updateModel()
        {
            if (getModel(false, false).Length > 0)
            {
                // Fix : Nécessité de vider le sprite avant d'en mettre un nouveau
                // Sinon le sprite est déformé avec le plus grand.
                _partList[selectedPart].PartImage.sprite = null;
                _partList[selectedPart].PartImage.sprite = getModel(true, false)[selectedModel];
            }
        }

        private void updateModelName()
        {
            Sprite[] listModel = getModel(false, false);
            String name = "-";
            if (listModel.Length > 0)
            {
                name = listModel[selectedModel].name;
            }

            modelText.text = name;
        }

        private void updateColor()
        {
            _partList[selectedPart].PartImage.color = getColor(true, false)[selectedColor];
        }

        private void generalColorUpdate()
        {
            _partList[0].PartImage.color = stuffColors[selectedColorJacket];
            _partList[1].PartImage.color = skinColors[selectedColorSkin];
            _partList[2].PartImage.color = stuffColors[selectedColorShirt];
            _partList[3].PartImage.color = stuffColors[selectedColorStuff];
            _partList[4].PartImage.color = hairColors[selectedColorHair];
            _partList[5].PartImage.color = skinColors[selectedColorSkin];
            _partList[6].PartImage.color = hairColors[selectedColorHair];
        }
        
        private void updateColorName()
        {
            Color[] listColor = getColor(false, false);
            Debug.Log(listColor.Length);
            String name = "-";
            if (listColor.Length > 0)
            {
                name = selectedColor.ToString();
            }

            colorText.text = name;
        }
    }
}
