﻿using System;
using UnityEngine.UI;

namespace Phase4MayorCreation
{
    public class MayorPart
    {
        public String PartName { get; set; }
        public Image PartImage { get; set; }

        public MayorPart(string partName, Image partImage)
        {
            PartName = partName;
            PartImage = partImage;
        }
    }
}
