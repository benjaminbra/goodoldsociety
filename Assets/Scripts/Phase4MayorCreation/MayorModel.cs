using System;
using UnityEngine.UI;

namespace Phase4MayorCreation
{
    public class MayorModel
    {
        public String ModelName { get; set; }
        public Image ModelImage { get; set; }

        public MayorModel(string modelName, Image modelImage)
        {
            ModelName = modelName;
            ModelImage = modelImage;
        }
    }
}