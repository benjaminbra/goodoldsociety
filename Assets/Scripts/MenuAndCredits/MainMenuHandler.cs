﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuHandler : MonoBehaviour
{
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCreditClick()
    {
        SceneManager.LoadScene("Credits");
    }
    
    public void OnNewGameClick()
    {
        SceneManager.LoadScene("Phase1");
    }
}
