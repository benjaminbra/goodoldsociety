﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoScroll : MonoBehaviour
{

    public Transform TextMeshPro;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TextMeshPro.transform.position += transform.up * Time.deltaTime;
    }

    public void OnRetourClick()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
