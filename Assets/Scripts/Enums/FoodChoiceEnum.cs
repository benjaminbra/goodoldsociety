namespace Enums
{
    public enum FoodChoiceEnum
    {
        Herbivore = 0,
        Omnivore = 1,
        Carnivore = 2,
    }
}