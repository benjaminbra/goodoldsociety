﻿using System;
using System.Collections;
using System.Collections.Generic;
using Enums;
using UnityEngine;

public class GameDataHandler : MonoBehaviour
{
    public int foodLevel;
    public FoodChoiceEnum foodChoice;
    public int aggressivityLevel;
    public int populationDensity;
    public int happinessLevel;
    public int temperatureLevel;
    public int money;
    public int pollutionLevel;
    
    public int maxFoodLevel;
    public int maxAggressivityLevel;
    public int maxPopulationDensity;
    public int maxHappinessLevel;
    public int maxTemperatureLevel;
    public int maxPollutionLevel;
    
    public bool isPollution;
    public bool IsHighDensity;
    public bool IsViolence;
    public bool IsDictator;
    public bool IsExctinction;
    public bool IsFamine;

    public bool isSelfDestruct;
    
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void updateHappiness(int level)
    {
     
        happinessLevel += level;

        if (happinessLevel > maxHappinessLevel)
        {
            happinessLevel = maxHappinessLevel;
        }
        else if (happinessLevel < 0)
        {
            happinessLevel = 0;
        }

        // Game Over de tristesse si à 0 ?

    }

    public void updateAggressivity(int level)
    {
        aggressivityLevel += level;
        
        if (aggressivityLevel > maxAggressivityLevel)
        {
            aggressivityLevel = maxAggressivityLevel;
        }
        else if (aggressivityLevel < 0)
        {
            aggressivityLevel = 0;
        }
    }

    public void updateMoney(int amount)
    {
        money += amount;
        if (money < 0)
        {
            money = 0;
        }
    }

    public void updatePollution(int level)
    {
        pollutionLevel += level;
        
        if (pollutionLevel > maxPollutionLevel)
        {
            pollutionLevel = maxPollutionLevel;
        }
        else if (pollutionLevel < 0)
        {
            pollutionLevel = 0;
        }
    }
    
    public void updatePopulation(int level)
    {
        populationDensity += level;
        
        if (populationDensity > maxPopulationDensity)
        {
            populationDensity = maxPopulationDensity;
        }
        else if (populationDensity < 0)
        {
            populationDensity = 0;
        }
    }

    public void updateFood(int level)
    {
        foodLevel += level;
        
        if (foodLevel > maxFoodLevel)
        {
            foodLevel = maxFoodLevel;
        }
        else if (foodLevel < 0)
        {
            foodLevel = 0;
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
