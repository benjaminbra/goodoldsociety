﻿using Enums;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Phase4Handler : MonoBehaviour
{
    public int killAggressivityUp = 30;
    public int bornPlanAmount = 500;
    public int invasionAggressivityUp = 30;
    public int vacuumPlanAmount = 7000;
    public int sorryLetterAmount = 1200;
    public int armyPlanAmmount = 2000;
    
    public InteractionWindowsVariant InteractionWindows;

    public GameDataHandler GameDataHandler;
    
    public GameObject carnivoreSprite;
    public GameObject omnivoreSprite;
    public GameObject herbivoreSprite;

    public Image FoodLevel;
    public Image TemperatureLevel;
    public Image MeanLevel;
    public Image PopulationLevel;
    public Image HapinesseLevel;
    public Image PollutionLevel;
    public TextMeshProUGUI moneyText;
    
    public GameObject Birds;
    public GameObject Sun;

    public GameObject CloudsPollution;
    public GameObject PollutionFilter;
    public GameObject UsineCheminee;

    public GameObject ImmeubleSimple1;
    public GameObject ImmeubleSimple2;
    public GameObject ImmeubleSimple3;

    public GameObject ImmeubleComplexe1;
    public GameObject ImmeubleComplexe2;
    public GameObject ImmeubleComplexe3;

    public GameObject Fire1;
    public GameObject Fire2;
    public GameObject Fire3;

    public AudioSource communistMusic;
    public GameObject DictatorCostume;
    
    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.FindGameObjectWithTag("GameDatas"))
        {
            GameDataHandler = GameObject.FindGameObjectWithTag("GameDatas").GetComponent<GameDataHandler>();
        }
        Invoke("renderIntroduction", 2.0f);
        if (GameDataHandler.foodChoice.Equals(FoodChoiceEnum.Carnivore))
        {
            carnivoreSprite.SetActive(true);
            herbivoreSprite.SetActive(false);
            omnivoreSprite.SetActive(false);
        } else if (GameDataHandler.foodChoice.Equals(FoodChoiceEnum.Herbivore))
        {
            carnivoreSprite.SetActive(false);
            herbivoreSprite.SetActive(true);
            omnivoreSprite.SetActive(false);
        } else if (GameDataHandler.foodChoice.Equals(FoodChoiceEnum.Omnivore))
        {
            carnivoreSprite.SetActive(false);
            herbivoreSprite.SetActive(false);
            omnivoreSprite.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        FoodLevel.fillAmount = (float)GameDataHandler.foodLevel / (float)GameDataHandler.maxFoodLevel;
        TemperatureLevel.fillAmount = (float)GameDataHandler.temperatureLevel / (float)GameDataHandler.maxTemperatureLevel;
        MeanLevel.fillAmount = (float)GameDataHandler.aggressivityLevel / (float)GameDataHandler.maxAggressivityLevel;
        PopulationLevel.fillAmount = (float)GameDataHandler.populationDensity / (float)GameDataHandler.maxPopulationDensity;
        HapinesseLevel.fillAmount = (float)GameDataHandler.happinessLevel / (float)GameDataHandler.maxHappinessLevel;
        PollutionLevel.fillAmount = (float)GameDataHandler.pollutionLevel / (float)GameDataHandler.maxPollutionLevel;
        moneyText.text = GameDataHandler.money.ToString();

        GameDataHandler.isPollution = PollutionLevel.fillAmount > 0.6f;
        GameDataHandler.IsHighDensity = PopulationLevel.fillAmount > 0.5f;
        GameDataHandler.IsViolence = MeanLevel.fillAmount > 0.75f;
        renderBackground();
    }

    public void renderIntroduction()
    {
        
        InteractionWindows.option1Button.interactable = true;
        InteractionWindows.option2Button.interactable = true;
        InteractionWindows.option3Button.interactable = true;
        InteractionWindows.gameObject.SetActive(false); 
        InteractionWindows.message.text = "Bonjour Leader ! " +
                                          "\r\nJe suis très heureux de vous voir ici dans mon bureau ! " +
                                          "\r\nJ'espère ne pas vous déranger. Mais je dois vous poser quelques questions." +
                                          "\r\nNe vous inquiétez pas, c'est purement en rapport avec la grandeur de notre espèce !";
        InteractionWindows.option1Button.onClick.RemoveAllListeners();
        InteractionWindows.option2Button.onClick.RemoveAllListeners();
        InteractionWindows.option3Button.onClick.RemoveAllListeners();
        InteractionWindows.option1Button.onClick.AddListener(renderQuestion1);
        InteractionWindows.option2Button.interactable = false;
        InteractionWindows.option3Button.onClick.AddListener(renderQuestion1);
        InteractionWindows.option1.text = "Avec plaisir, je vous écoute.";
        InteractionWindows.option2.text = "";
        InteractionWindows.option3.text = "D'accord mais faites vite s'il vous plait !";
        InteractionWindows.gameObject.SetActive(true); 
    }
    
    public void renderQuestion1()
    {
        
        InteractionWindows.option1Button.interactable = true;
        InteractionWindows.option2Button.interactable = true;
        InteractionWindows.option3Button.interactable = true;
        InteractionWindows.gameObject.SetActive(false);
        InteractionWindows.message.text = "Ma première question est en rapport avec notre population." +
                                          "\r\nNotre densité est actuellement à " + (((float)GameDataHandler.populationDensity / (float)GameDataHandler.maxPopulationDensity) * 100f) + " %." +
                                          "\r\nQue souhaitez vous faire ?";
        InteractionWindows.option1Button.onClick.RemoveAllListeners();
        InteractionWindows.option2Button.onClick.RemoveAllListeners();
        InteractionWindows.option3Button.onClick.RemoveAllListeners();
        InteractionWindows.option1Button.onClick.AddListener(killQuestion1);
        InteractionWindows.option2Button.onClick.AddListener(renderQuestion2);
        InteractionWindows.option2Button.interactable = true;
        if (GameDataHandler.money < bornPlanAmount)
        {
            InteractionWindows.option3Button.interactable = false;
        }
        else
        {
            InteractionWindows.option3Button.onClick.AddListener(bornQuestion1);
        }
        InteractionWindows.option1.text = "Nous devons exterminer la moitié de la population ! (+ "+ killAggressivityUp +" Aggressivité)";
        InteractionWindows.option2.text = "Ne rien faire";
        InteractionWindows.option3.text = "Il faut faire une campagne d'encouragement de la reproduction ! (- " + bornPlanAmount + " Or)";
        InteractionWindows.gameObject.SetActive(true); 
    }

    public void killQuestion1()
    {
        
        InteractionWindows.gameObject.SetActive(false);
        GameDataHandler.updateAggressivity(killAggressivityUp);
        GameDataHandler.updatePopulation(-1 * Mathf.RoundToInt(GameDataHandler.populationDensity * 0.5f));
        Invoke("renderQuestion2", 2.0f);
    }

    public void bornQuestion1()
    {
        
        InteractionWindows.gameObject.SetActive(false);
        GameDataHandler.updateMoney(-1 * bornPlanAmount);
        GameDataHandler.updatePopulation(Mathf.RoundToInt(GameDataHandler.populationDensity * 0.25f));
        Invoke("renderQuestion2", 2.0f);
    }
    
    public void renderQuestion2()
    {
        InteractionWindows.option1Button.interactable = true;
        InteractionWindows.option2Button.interactable = true;
        InteractionWindows.option3Button.interactable = true;
        InteractionWindows.message.text = "Ma seconde question est en rapport avec la pollution." +
                                          "\r\nLe taux de pollution est actuellement à " + (((float)GameDataHandler.pollutionLevel / (float)GameDataHandler.maxPollutionLevel) * 100f) + " %." +
                                          "\r\nQue souhaitez vous faire ?";
        InteractionWindows.option1Button.onClick.RemoveAllListeners();
        InteractionWindows.option2Button.onClick.RemoveAllListeners();
        InteractionWindows.option3Button.onClick.RemoveAllListeners();
        InteractionWindows.option1Button.onClick.AddListener(invasionQuestion2);
        InteractionWindows.option2Button.onClick.AddListener(renderQuestion3);
        if (GameDataHandler.money < vacuumPlanAmount)
        {
            InteractionWindows.option3Button.interactable = false;
        }
        else
        {
            InteractionWindows.option3Button.interactable = true;
            InteractionWindows.option3Button.onClick.AddListener(vacuumQuestion2);
        }
        InteractionWindows.option1.text = "Envahir un continent non pollué ! (+ "+ invasionAggressivityUp +" Aggressivité)";
        InteractionWindows.option2.text = "Ne rien faire";
        InteractionWindows.option3.text = "Il faut construire un aspirateur géant ! (- "+ vacuumPlanAmount +" Or)";
        InteractionWindows.gameObject.SetActive(true); 
    }

    public void invasionQuestion2()
    {
        
        InteractionWindows.gameObject.SetActive(false);
        GameDataHandler.updateAggressivity(invasionAggressivityUp);
        GameDataHandler.updatePollution(-1* Mathf.RoundToInt(GameDataHandler.pollutionLevel * 0.8f));
        Invoke("renderQuestion3", 2.0f);
    }

    public void vacuumQuestion2()
    {
        
        InteractionWindows.gameObject.SetActive(false);
        GameDataHandler.updateMoney(-1 * vacuumPlanAmount);
        GameDataHandler.updatePollution(-1* Mathf.RoundToInt(GameDataHandler.pollutionLevel * 0.5f));
        Invoke("renderQuestion3", 2.0f);
    }
    
    public void renderQuestion3()
    {
        InteractionWindows.option1Button.interactable = true;
        InteractionWindows.option2Button.interactable = true;
        InteractionWindows.option3Button.interactable = true;
        InteractionWindows.message.text = "Ma troisième et dernière question est en rapport avec la violence." +
                                          "\r\nLe taux de violence est actuellement à " + (((float)GameDataHandler.aggressivityLevel / (float)GameDataHandler.maxAggressivityLevel) * 100f) + " %." +
                                          "\r\nQue souhaitez vous faire ?";
        InteractionWindows.option1Button.onClick.RemoveAllListeners();
        InteractionWindows.option2Button.onClick.RemoveAllListeners();
        InteractionWindows.option3Button.onClick.RemoveAllListeners();
        if (GameDataHandler.money < armyPlanAmmount)
        {
            InteractionWindows.option1Button.interactable = false;
        }
        else
        {
            InteractionWindows.option1Button.interactable = true;
            InteractionWindows.option1Button.onClick.AddListener(ArmyQuestion3);
        }
        
        InteractionWindows.option2Button.onClick.AddListener(renderConclusion);
        if (GameDataHandler.money < sorryLetterAmount)
        {
            InteractionWindows.option3Button.interactable = false;
        }
        else
        {
            InteractionWindows.option3Button.interactable = true;
            InteractionWindows.option3Button.onClick.AddListener(LetterQuestion3);
        }
        InteractionWindows.option1.text = "Renforcer l'armée et surveiller la population (- "+ armyPlanAmmount +" Or + Nouveau rang)";
        InteractionWindows.option2.text = "Ne rien faire";
        InteractionWindows.option3.text = "Envoyer des lettres d'excuses ! (- "+ sorryLetterAmount +" Or)";
        InteractionWindows.gameObject.SetActive(true); 
    }

    public void ArmyQuestion3()
    {
        
        InteractionWindows.gameObject.SetActive(false);
        GameDataHandler.updateMoney(-1 * armyPlanAmmount);
        GameDataHandler.IsDictator = true;
        GameDataHandler.updateAggressivity(-1* Mathf.RoundToInt(GameDataHandler.aggressivityLevel * 0.8f));
        Invoke("renderConclusion", 2.0f);
    }

    public void LetterQuestion3()
    {
        
        InteractionWindows.gameObject.SetActive(false);
        GameDataHandler.updateMoney(-1 * sorryLetterAmount);
        GameDataHandler.updateAggressivity(-1* Mathf.RoundToInt(GameDataHandler.aggressivityLevel * 0.5f));
        Invoke("renderConclusion", 2.0f);
    }
    
    public void renderConclusion()
    {
        InteractionWindows.gameObject.SetActive(false);
        if (GameDataHandler.IsDictator)
        {
            InteractionWindows.message.text = "Merci Leader Suprême ! " +
                                              "\r\nJe ne vous embête pas plus longtemps Suprême Leader ! " +
                                              "\r\nJe vais désormais travailler dur pour améliorer notre espèce !" +
                                              "\r\nAu revoir Suprême Leader !";
        }
        else
        {
            InteractionWindows.message.text = "Merci Leader ! " +
                                              "\r\nJe ne vous embête pas plus longtemps ! " +
                                              "\r\nOH ! C'est l'heure de la pause café !" +
                                              "\r\nAu revoir !";
        }

        InteractionWindows.option1Button.onClick.RemoveAllListeners();
        InteractionWindows.option2Button.onClick.RemoveAllListeners();
        InteractionWindows.option3Button.onClick.RemoveAllListeners();
        InteractionWindows.option1Button.interactable = false;
        InteractionWindows.option2Button.onClick.AddListener(goEndPhase);
        InteractionWindows.option2Button.interactable = true;
        InteractionWindows.option3Button.interactable = false;
        InteractionWindows.option1.text = "";
        InteractionWindows.option2.text = "Au revoir";
        InteractionWindows.option3.text = "";
        InteractionWindows.gameObject.SetActive(true); 
    }

    public void goEndPhase()
    {
        SceneManager.LoadScene("Ending");
    }
    
    public void renderBackground()
    {
        if (GameDataHandler.isPollution)
        {
            Birds.SetActive(false);
            Sun.SetActive(false);
            CloudsPollution.SetActive(true);
            PollutionFilter.SetActive(true);
            UsineCheminee.SetActive(true);
        }
        else
        {
            Birds.SetActive(true);
            Sun.SetActive(true);
            CloudsPollution.SetActive(false);
            PollutionFilter.SetActive(false);
            UsineCheminee.SetActive(false);
        }

        if (GameDataHandler.IsHighDensity)
        {
            ImmeubleSimple1.SetActive(true);
            ImmeubleSimple2.SetActive(true);
            ImmeubleSimple3.SetActive(true);
            ImmeubleComplexe1.SetActive(true);
            ImmeubleComplexe2.SetActive(true);
            ImmeubleComplexe3.SetActive(true);
            if (GameDataHandler.IsViolence)
            {
                Fire1.SetActive(true);
                Fire2.SetActive(true);
                Fire3.SetActive(true);
            }
            else
            {
                Fire1.SetActive(false);
                Fire2.SetActive(false);
                Fire3.SetActive(false);  
            }
        }
        else
        {
            
            ImmeubleSimple1.SetActive(false);
            ImmeubleSimple2.SetActive(true);
            ImmeubleSimple3.SetActive(true);
            ImmeubleComplexe1.SetActive(false);
            ImmeubleComplexe2.SetActive(false);
            ImmeubleComplexe3.SetActive(false);
            if (GameDataHandler.IsViolence)
            {
                Fire1.SetActive(false);
                Fire2.SetActive(true);
                Fire3.SetActive(false);
            }
            else
            {
                Fire1.SetActive(false);
                Fire2.SetActive(false);
                Fire3.SetActive(false);  
            }
        }

        if (GameDataHandler.IsDictator)
        {
            DictatorCostume.SetActive(true);
            if (!communistMusic.isPlaying)
            {
                communistMusic.Play();
            }
        }
        else
        {
            DictatorCostume.SetActive(false);
            communistMusic.Stop();
        }
    }
}
