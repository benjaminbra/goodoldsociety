﻿using UnityEngine;

public class MovingPoints : MonoBehaviour
{
   
    public MovingPoints parentPoint { get; set; }

    public MovingPoints[] childPoints;

    public bool isLeaderPresent;

    public bool isConquered;

    public bool isUsineInstalled;

    public GameObject imageConquered;

    public GameObject imageLeader;

    public GameObject imageCapitale;

    public GameObject imageUsine;
    
    public LineRenderer lineToParent;

    public Material lineRenderer;
    
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (imageCapitale != null)
        {
            imageCapitale.SetActive(isConquered);
        }

        if (imageUsine != null)
        {
            imageUsine.SetActive(isUsineInstalled);
        }
        imageConquered.SetActive(isConquered);
        imageLeader.SetActive(isLeaderPresent);

    }

    public void removeChildRoads()
    {
        for (int i = 0; i < childPoints.Length; i++)
        {
            childPoints[i].lineToParent = null;
            Destroy(childPoints[i].gameObject.GetComponent<LineRenderer>());
        }
    }
    
    public void displayChildUnConqueredRoads()
    {
        for (int i = 0; i < childPoints.Length; i++)
        {
            if (!childPoints[i].isConquered)
            {
                if (childPoints[i].lineToParent != null)
                {
                    childPoints[i].lineToParent.SetPosition(childPoints[i].lineToParent.positionCount-1, transform.position);
                }
                else
                {
                    childPoints[i].lineToParent = childPoints[i].gameObject.AddComponent<LineRenderer>();
                    childPoints[i].lineToParent.startWidth = 0.253f;
                    childPoints[i].lineToParent.endWidth = 0.253f;
                    childPoints[i].lineToParent.material = lineRenderer;
                    childPoints[i].lineToParent.colorGradient = setGradientColor();
                    childPoints[i].lineToParent.SetPositions(new Vector3[] {transform.position, childPoints[i].transform.position});
                }

            }   
        }
    }

    public void displayChildAllConqueredRoads()
    {
        for (int i = 0; i < childPoints.Length; i++)
        {
            if (childPoints[i].isConquered)
            {
                if (childPoints[i].lineToParent != null)
                {
                    childPoints[i].lineToParent.SetPosition(childPoints[i].lineToParent.positionCount-1, transform.position);
                }
                else
                {
                    childPoints[i].lineToParent = childPoints[i].gameObject.AddComponent<LineRenderer>();
                    childPoints[i].lineToParent.startWidth = 0.253f;
                    childPoints[i].lineToParent.endWidth = 0.253f;
                    childPoints[i].lineToParent.material = lineRenderer;
                    childPoints[i].lineToParent.colorGradient = setGradientColor();
                    childPoints[i].lineToParent.SetPositions(new Vector3[] {transform.position, childPoints[i].transform.position});
                }

            }   
        }
    }

    
    public Gradient setGradientColor()
    {
        Gradient gradient = new Gradient();

        // Populate the color keys at the relative time 0 and 1 (0 and 100%)
        GradientColorKey[] colorKey = new GradientColorKey[2];
        colorKey[0].color = Color.grey;
        colorKey[0].time = 0.0f;
        colorKey[1].color = Color.grey;
        colorKey[1].time = 1.0f;

        // Populate the alpha  keys at relative time 0 and 1  (0 and 100%)
        GradientAlphaKey[] alphaKey = new GradientAlphaKey[2];
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 1.0f;
        alphaKey[1].time = 1.0f;

        gradient.SetKeys(colorKey, alphaKey);

        return gradient;
    }

    public bool hasMovingPointUnconqueredChild(MovingPoints point)
    {
        bool hasChild = false;
        foreach (var childPoint in childPoints)
        {
            if (childPoint == point && !childPoint.isConquered)
            {
                hasChild = true;
                break;
            }
        }

        return hasChild;
    }
}
