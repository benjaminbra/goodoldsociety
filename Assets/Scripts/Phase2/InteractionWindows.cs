﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InteractionWindows : MonoBehaviour
{
    public TextMeshProUGUI message;
    public TextMeshProUGUI validate;
    public TextMeshProUGUI cancel;
    public Button valideButton;
    public Button cancelButton;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
