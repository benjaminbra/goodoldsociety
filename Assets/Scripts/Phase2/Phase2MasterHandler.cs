﻿using Enums;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Phase2MasterHandler : MonoBehaviour
{
    public bool isPhase3;
    public int phase3maxTimerSecondes;
    public TextMeshProUGUI moneyText;
    public GameObject MoneyUI;
    public int tradeAmount = 500;
    public int buyCityAmount = 10000;
    public int usineBuildAmount = 100;
    public int usineDestroyAmount = 10;
    public int usinePowerMoney = 7;
    public int dailyMoney = 10;
    
    public float actualTimer;
    public float maxTimerSecondes;
    public RectTransform aiguille;
    
    public GameDataHandler GameDataHandler;

    public MovingPoints actualPlayerPosition;

    public MovingPoints[] spawnPoints;
    
    private MovingPoints nextPoint;

    public InteractionWindows InteractionWindows;

    private MovingPoints[] playerCityList;
    private int listIndex = 0;
    
    public bool isLeaderMoving;
    private int actualLeaderPosition = 0;
    public int travelTime = 5;
    public int attackedProbability = 100;
    public int encounteredProbability = 10;
    public int deathProbability = 10;
    
    public GameObject carnivoreSprite;
    public GameObject omnivoreSprite;
    public GameObject herbivoreSprite;
    public Image FoodLevel;
    public Image TemperatureLevel;
    public Image MeanLevel;
    public Image PopulationLevel;
    public Image HapinesseLevel;
    public Image PollutionLevel;
    public GameObject PollutionUI;
    
    // Start is called before the first frame update
    void Start()
    {
        MoneyUI.SetActive(false);
        PollutionUI.SetActive(false);
        playerCityList = new MovingPoints[8];
        InvokeRepeating("addSecondAtTimer", 1f, 1f);
            
        if (GameObject.FindGameObjectWithTag("GameDatas"))
        {
            GameDataHandler = GameObject.FindGameObjectWithTag("GameDatas").GetComponent<GameDataHandler>();
        }

        actualPlayerPosition = spawnPoints[Random.Range(0, spawnPoints.Length)];
        actualPlayerPosition.isConquered = true;
        playerCityList[listIndex] = actualPlayerPosition;
        listIndex++;
        actualPlayerPosition.isLeaderPresent = true;
        actualPlayerPosition.displayChildUnConqueredRoads();
        foreach (var spawnPoint in spawnPoints)
        {
            if (spawnPoint != actualPlayerPosition)
            {
                Destroy(spawnPoint.gameObject);
            }
        }

        if (GameDataHandler.foodChoice.Equals(FoodChoiceEnum.Carnivore))
        {
            carnivoreSprite.SetActive(true);
            herbivoreSprite.SetActive(false);
            omnivoreSprite.SetActive(false);
        } else if (GameDataHandler.foodChoice.Equals(FoodChoiceEnum.Herbivore))
        {
            carnivoreSprite.SetActive(false);
            herbivoreSprite.SetActive(true);
            omnivoreSprite.SetActive(false);
        } else if (GameDataHandler.foodChoice.Equals(FoodChoiceEnum.Omnivore))
        {
            carnivoreSprite.SetActive(false);
            herbivoreSprite.SetActive(false);
            omnivoreSprite.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameDataHandler.foodLevel == 0)
        {
            GameDataHandler.IsFamine = true;
            SceneManager.LoadScene("Ending");
        }
        
        if (actualTimer >= maxTimerSecondes)
        {
            if (!isPhase3)
            {
                // Passage à la phase 3
                isPhase3 = true;
                actualTimer = 0;
                maxTimerSecondes = phase3maxTimerSecondes;
                MoneyUI.SetActive(true);
                PollutionUI.SetActive(true);
                for (int i=0; i < playerCityList.Length; i++)
                {
                    if (playerCityList[i] != null)
                    {
                        playerCityList[i].removeChildRoads();
                        playerCityList[i].isLeaderPresent = false;
                        if (playerCityList[i].imageCapitale != null)
                        {
                            playerCityList[i].isLeaderPresent = true;
                        }
                    }
                    
                }
                for (int i=0; i < playerCityList.Length; i++)
                {
                    if (playerCityList[i] != null)
                    {
                        playerCityList[i].displayChildAllConqueredRoads();
                    }
                    
                }
            }
            else
            {
                SceneManager.LoadScene("Phase4");
            }
        }

        int aiguilleRotation = Mathf.RoundToInt(-1f * ((actualTimer/maxTimerSecondes)*360f));
        aiguille.localEulerAngles = new Vector3(0,0, aiguilleRotation);
        
        FoodLevel.fillAmount = (float)GameDataHandler.foodLevel / (float)GameDataHandler.maxFoodLevel;
        TemperatureLevel.fillAmount = (float)GameDataHandler.temperatureLevel / (float)GameDataHandler.maxTemperatureLevel;
        MeanLevel.fillAmount = (float)GameDataHandler.aggressivityLevel / (float)GameDataHandler.maxAggressivityLevel;
        PopulationLevel.fillAmount = (float)GameDataHandler.populationDensity / (float)GameDataHandler.maxPopulationDensity;
        HapinesseLevel.fillAmount = (float)GameDataHandler.happinessLevel / (float)GameDataHandler.maxHappinessLevel;
        PollutionLevel.fillAmount = (float)GameDataHandler.pollutionLevel / (float)GameDataHandler.maxPollutionLevel;
    }

    public void OnClickSpawnPoint(GameObject i)
    {
        
    }

    public void onClickCampPoint(GameObject i)
    {
        if (!isPhase3)
        {
            bool hasUnconqueredChild = actualPlayerPosition.hasMovingPointUnconqueredChild(i.GetComponent<MovingPoints>());
            if (isLeaderMoving)
            {
                InteractionWindows.message.text = "Votre leader est déjà entrain de se déplacer. \r\nAttendez qu'il meurt ou qu'il arrive à destination.";
                InteractionWindows.cancelButton.onClick.RemoveAllListeners();
                InteractionWindows.valideButton.onClick.RemoveAllListeners();
                InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
                InteractionWindows.cancel.text = "Fermer";
                InteractionWindows.validate.text = "";
                InteractionWindows.valideButton.interactable = false;
                InteractionWindows.gameObject.SetActive(true); 
            } else if (!hasUnconqueredChild)
            {
                InteractionWindows.message.text = "Vous ne pouvez pas atteindre ce campement.";
                InteractionWindows.cancelButton.onClick.RemoveAllListeners();
                InteractionWindows.valideButton.onClick.RemoveAllListeners();
                InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
                InteractionWindows.cancel.text = "Fermer";
                InteractionWindows.validate.text = "";
                InteractionWindows.valideButton.interactable = false;
                InteractionWindows.gameObject.SetActive(true); 
            }
            else
            {
                InteractionWindows.message.text = "Êtes-vous sûr de vouloir partir dans ce campement ? \r\nVous ne pourrez plus revenir ensuite.";
                InteractionWindows.cancelButton.onClick.RemoveAllListeners();
                InteractionWindows.valideButton.onClick.RemoveAllListeners();
                InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
                InteractionWindows.cancel.text = "Non";
                InteractionWindows.validate.text = "Oui";
                InteractionWindows.valideButton.onClick.AddListener(() => runLeaderMovement(i.GetComponent<MovingPoints>()));
                InteractionWindows.valideButton.interactable = true;
                InteractionWindows.gameObject.SetActive(true); 
            } 
        }
        else
        {
            MovingPoints clickedPlayer = i.GetComponent<MovingPoints>();
            if (clickedPlayer.isConquered)
            {
                if (clickedPlayer.isUsineInstalled)
                {
                    InteractionWindows.message.text = "Les usines polluent beaucoup trop ! \r\n Acceptez-vous de retirer cette usine ? (" + usineDestroyAmount + " Or)";
                    InteractionWindows.cancelButton.onClick.RemoveAllListeners();
                    InteractionWindows.valideButton.onClick.RemoveAllListeners();
                    InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
                    InteractionWindows.valideButton.onClick.AddListener(() => destroyUsine(i.GetComponent<MovingPoints>()));
                    InteractionWindows.cancel.text = "Non";
                    InteractionWindows.validate.text = "Oui";
                    InteractionWindows.valideButton.interactable = true;
                    InteractionWindows.gameObject.SetActive(true); 
                }
                else
                {
                    InteractionWindows.message.text = "Vous n'avez pas encore d'usine dans votre ville. \r\nSouhaitez vous en l'installer ? (" + usineBuildAmount + " Or)";
                    InteractionWindows.cancelButton.onClick.RemoveAllListeners();
                    InteractionWindows.valideButton.onClick.RemoveAllListeners();
                    InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
                    InteractionWindows.valideButton.onClick.AddListener(() => buyUsine(i.GetComponent<MovingPoints>()));
                    InteractionWindows.cancel.text = "Non";
                    InteractionWindows.validate.text = "Oui";
                    InteractionWindows.valideButton.interactable = true;
                    InteractionWindows.gameObject.SetActive(true); 
                    
                }
            }
            else
            {
                InteractionWindows.message.text = "Vous vous retrouvez face au Leader de la ville. \r\nQue faites vous ?";
                InteractionWindows.cancelButton.onClick.RemoveAllListeners();
                InteractionWindows.valideButton.onClick.RemoveAllListeners();
                InteractionWindows.cancelButton.onClick.AddListener(tradeWithCityDialogue);
                InteractionWindows.valideButton.onClick.AddListener(() => buyCityDialogue(i.GetComponent<MovingPoints>()));
                InteractionWindows.cancel.text = "Faire du commerce avec la ville";
                InteractionWindows.validate.text = "Acheter la ville et ses habitants";
                InteractionWindows.valideButton.interactable = true;
                InteractionWindows.gameObject.SetActive(true); 
            }
        }
    }

    public void buyUsine(MovingPoints city)
    {
        if (GameDataHandler.money < usineBuildAmount)
        {
            InteractionWindows.gameObject.SetActive(false); 
            InteractionWindows.message.text = "Patron ! Les coffres sont vides !";
            InteractionWindows.cancelButton.onClick.RemoveAllListeners();
            InteractionWindows.valideButton.onClick.RemoveAllListeners();
            InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
            InteractionWindows.cancel.text = "Fermer";
            InteractionWindows.validate.text = "";
            InteractionWindows.valideButton.interactable = false;
            InteractionWindows.gameObject.SetActive(true); 
        }
        else
        {
            GameDataHandler.updateMoney(-1 * usineBuildAmount);
            city.isUsineInstalled = true;
            
            InteractionWindows.gameObject.SetActive(false); 
            InteractionWindows.message.text = "Félicitations, nous avons désormais une magnifique usine Polumax3000 !";
            InteractionWindows.cancelButton.onClick.RemoveAllListeners();
            InteractionWindows.valideButton.onClick.RemoveAllListeners();
            InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
            InteractionWindows.cancel.text = "Fermer";
            InteractionWindows.validate.text = "";
            InteractionWindows.valideButton.interactable = false;
            InteractionWindows.gameObject.SetActive(true); 
        }
    }

    public void destroyUsine(MovingPoints city)
    {
        if (GameDataHandler.money < usineDestroyAmount)
        {
            InteractionWindows.gameObject.SetActive(false); 
            InteractionWindows.message.text = "Patron ! Les coffres sont vides !";
            InteractionWindows.cancelButton.onClick.RemoveAllListeners();
            InteractionWindows.valideButton.onClick.RemoveAllListeners();
            InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
            InteractionWindows.cancel.text = "Fermer";
            InteractionWindows.validate.text = "";
            InteractionWindows.valideButton.interactable = false;
            InteractionWindows.gameObject.SetActive(true); 
        }
        else
        {
            GameDataHandler.updateMoney(-1 * usineDestroyAmount);
            city.isUsineInstalled = false;
            
            InteractionWindows.gameObject.SetActive(false); 
            InteractionWindows.message.text = "Félicitations, nous avons désormais de magnifiques ruines !";
            InteractionWindows.cancelButton.onClick.RemoveAllListeners();
            InteractionWindows.valideButton.onClick.RemoveAllListeners();
            InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
            InteractionWindows.cancel.text = "Fermer";
            InteractionWindows.validate.text = "";
            InteractionWindows.valideButton.interactable = false;
            InteractionWindows.gameObject.SetActive(true); 
        }
    }

    public void tradeWithCityDialogue()
    {
        InteractionWindows.gameObject.SetActive(false); 
        InteractionWindows.message.text = "Êtes vous sûr de vouloir échanger avec ces malotrus ? (" + tradeAmount + " Or)";
        InteractionWindows.cancelButton.onClick.RemoveAllListeners();
        InteractionWindows.valideButton.onClick.RemoveAllListeners();
        InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
        InteractionWindows.valideButton.onClick.AddListener(tradeWithCity);
        InteractionWindows.cancel.text = "Non";
        InteractionWindows.validate.text = "Oui";
        InteractionWindows.valideButton.interactable = true;
        InteractionWindows.gameObject.SetActive(true); 
    }

    public void tradeWithCity()
    {
        if (GameDataHandler.money < tradeAmount)
        {
            InteractionWindows.gameObject.SetActive(false); 
            InteractionWindows.message.text = "Sale pauvre ! Vous n'avez pas assez d'argent !";
            InteractionWindows.cancelButton.onClick.RemoveAllListeners();
            InteractionWindows.valideButton.onClick.RemoveAllListeners();
            InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
            InteractionWindows.cancel.text = "Fermer";
            InteractionWindows.validate.text = "";
            InteractionWindows.valideButton.interactable = false;
            InteractionWindows.gameObject.SetActive(true); 
        }
        else
        {
            GameDataHandler.updateAggressivity(-5);
            GameDataHandler.updateMoney(-1 * tradeAmount);
            
            InteractionWindows.gameObject.SetActive(false); 
            InteractionWindows.message.text = "Félicitations, la transaction s'est parfaitement déroulée !";
            InteractionWindows.cancelButton.onClick.RemoveAllListeners();
            InteractionWindows.valideButton.onClick.RemoveAllListeners();
            InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
            InteractionWindows.cancel.text = "Fermer";
            InteractionWindows.validate.text = "";
            InteractionWindows.valideButton.interactable = false;
            InteractionWindows.gameObject.SetActive(true); 
        }
    }
    
    public void buyCityDialogue(MovingPoints city)
    {
        InteractionWindows.gameObject.SetActive(false); 
        InteractionWindows.message.text = "Êtes vous sûr de vouloir acheter une ville aussi hideuse ? (" + buyCityAmount + " Or)";
        InteractionWindows.cancelButton.onClick.RemoveAllListeners();
        InteractionWindows.valideButton.onClick.RemoveAllListeners();
        InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
        InteractionWindows.valideButton.onClick.AddListener(() => buyCity(city));
        InteractionWindows.cancel.text = "Non";
        InteractionWindows.validate.text = "Oui";
        InteractionWindows.valideButton.interactable = true;
        InteractionWindows.gameObject.SetActive(true); 
    }
    
    public void buyCity(MovingPoints city)
    {
        if (GameDataHandler.money < buyCityAmount)
        {
            InteractionWindows.gameObject.SetActive(false); 
            InteractionWindows.message.text = "Sale pauvre ! Vous n'avez pas assez d'argent !";
            InteractionWindows.cancelButton.onClick.RemoveAllListeners();
            InteractionWindows.valideButton.onClick.RemoveAllListeners();
            InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
            InteractionWindows.cancel.text = "Fermer";
            InteractionWindows.validate.text = "";
            InteractionWindows.valideButton.interactable = false;
            InteractionWindows.gameObject.SetActive(true); 
        }
        else
        {
            GameDataHandler.updateAggressivity(10);
            GameDataHandler.updateMoney(-1 * buyCityAmount);
            city.isConquered = true;
            city.displayChildAllConqueredRoads();
            playerCityList[listIndex] = city;
            
            InteractionWindows.gameObject.SetActive(false); 
            InteractionWindows.message.text = "Merci ! J'en avais marre de diriger ces gros nuls !";
            InteractionWindows.cancelButton.onClick.RemoveAllListeners();
            InteractionWindows.valideButton.onClick.RemoveAllListeners();
            InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
            InteractionWindows.cancel.text = "Fermer";
            InteractionWindows.validate.text = "";
            InteractionWindows.valideButton.interactable = false;
            InteractionWindows.gameObject.SetActive(true); 
        }
    }
    
    public void cancelAndCloseWindow()
    {
        InteractionWindows.gameObject.SetActive(false);
    }

    public void runLeaderMovement(MovingPoints aimedPoint)
    {
        isLeaderMoving = true;
        InteractionWindows.gameObject.SetActive(false); 
        nextPoint = aimedPoint;
        Invoke("movingLeaderToPoint", 1.0f);
        
    }

    public void movingLeaderToPoint()
    {
        if (!isPhase3)
        {
            actualLeaderPosition++;
            if (actualLeaderPosition == travelTime)
            {
                actualLeaderPosition = 0;
                isLeaderArrived();
            }
            else
            {
                bool isEncountered = Random.Range(0, encounteredProbability) == 0;
                if (isEncountered)
                {
                    showPlayerIsEncountered();
                }
                else
                {
                    Invoke("movingLeaderToPoint", 1.0f);
                }
            }
        }
        
    }

    public void showPlayerIsEncountered()
    {
        InteractionWindows.message.text = "Votre leader fais la rencontre d'autres personnes. \r\nQue souhaitez vous faire ?";
        InteractionWindows.cancelButton.onClick.RemoveAllListeners();
        InteractionWindows.valideButton.onClick.RemoveAllListeners();
        InteractionWindows.cancelButton.onClick.AddListener(playerIsChatting);
        InteractionWindows.valideButton.onClick.AddListener(() => playerIsAttacking(true));
        InteractionWindows.cancel.text = "Discuter";
        InteractionWindows.validate.text = "Attaquer";
        InteractionWindows.valideButton.interactable = true;
        InteractionWindows.gameObject.SetActive(true);
    }
    
    public void playerIsAttacking(bool fromPlayer)
    {
        InteractionWindows.gameObject.SetActive(false); 
        if (fromPlayer)
        {
            // Si le player est virulent, son aggressivité est augmentée.
            GameDataHandler.updateAggressivity(1);
        }
        
        bool isDead = Random.Range(0, deathProbability) == 0;
        if (isDead)
        {
            isLeaderMoving = false;
            actualLeaderPosition = 0;
            GameDataHandler.updateHappiness(10);
            displayDeadLeaderMessage();
        }
        else
        {
            if (!fromPlayer)
            {
                displayGotAttackedMessage();
            }
            Invoke("movingLeaderToPoint", 1.0f);
        }
        
    }

    public void displayDeadLeaderMessage()
    {
        InteractionWindows.message.text = "Votre leader s'est fait massacrer. \r\nUn nouveau leader a donc été nommé. \r\nEspérons qu'il ait plus de chances que sont prédécesseur.";
        InteractionWindows.cancelButton.onClick.RemoveAllListeners();
        InteractionWindows.valideButton.onClick.RemoveAllListeners();
        InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
        InteractionWindows.cancel.text = "Fermer";
        InteractionWindows.validate.text = "";
        InteractionWindows.valideButton.interactable = false;
        InteractionWindows.gameObject.SetActive(true);
    }
    
    public void displayGotAttackedMessage()
    {
        InteractionWindows.message.text = "La conversation a mal tournée et vous en êtes venus aux mains. \r\nHeureusement vous étiez plus fort que votre adversaire.";
        InteractionWindows.cancelButton.onClick.RemoveAllListeners();
        InteractionWindows.valideButton.onClick.RemoveAllListeners();
        InteractionWindows.cancelButton.onClick.AddListener(cancelAndCloseWindow);
        InteractionWindows.cancel.text = "Fermer";
        InteractionWindows.validate.text = "";
        InteractionWindows.valideButton.interactable = false;
        InteractionWindows.gameObject.SetActive(true);
    }
    
    public void playerIsChatting()
    {
        InteractionWindows.gameObject.SetActive(false); 
        bool isAttacked = Random.Range(0, attackedProbability - GameDataHandler.aggressivityLevel) == 0;
        if (isAttacked)
        {
            playerIsAttacking(false);
        }
        else
        {
            Invoke("movingLeaderToPoint", 1.0f);
        }
    }
    
    public void isLeaderArrived()
    {
        actualPlayerPosition.removeChildRoads();
        isLeaderMoving = false;
        actualPlayerPosition.isLeaderPresent = false;
        actualPlayerPosition = nextPoint;
        actualPlayerPosition.isConquered = true;
        playerCityList[listIndex] = actualPlayerPosition;
        listIndex++;
        actualPlayerPosition.isLeaderPresent = true;
        actualPlayerPosition.displayChildUnConqueredRoads();
        nextPoint = null;
        GameDataHandler.updatePopulation(Mathf.RoundToInt(GameDataHandler.populationDensity * -0.25f));
    }
    
    void addSecondAtTimer()
    {
        actualTimer += 1;
        if (!isPhase3)
        {
            GameDataHandler.updatePopulation(1);
        }
        else
        {
            moneyText.text = GameDataHandler.money.ToString();
            for (int i=0; i < playerCityList.Length; i++)
            {
                int moneyToGet = dailyMoney;
                if (playerCityList[i] != null && playerCityList[i].isUsineInstalled)
                {
                    moneyToGet *= usinePowerMoney;
                    GameDataHandler.updatePollution(2);
                    GameDataHandler.updatePopulation(1);
                }
                GameDataHandler.updateMoney(moneyToGet);

            }
        }
        GameDataHandler.foodLevel = GameDataHandler.maxFoodLevel-GameDataHandler.populationDensity;
        
    }
}
